# Ansible Role: Zextras Carbonio Community Edition

This role installs [Zextras Carbonio Community Edition](https://www.zextras.com/carbonio-community-edition/) package on any supported host.

## TODO:

- bootstrap the server with the appropriate variables.
- change zextras password.

## Requirements

### Hardware

|   |   |
|---|---|
| CPU  | Intel/AMD 64-bit CPU 1.5 GHz  |
| RAM  | 8 GB  |
| Disk space (Operating system and Carbonio CE)  | 40 GB |

### Software

- Carbonio CE is available for 64-bit CPUs only and can be installed on Ubuntu 18.04 LTS Server Edition.

### DNS Resolution

Carbonio CE needs valid DNS resolution for:

- the domain (MX and A record)

- the FQDN (A record)

So make sure that the DNS is correctly configured for both A and MX records.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    carbonio_gpgkey_id: 52FD40243E584A21
    carbonio_repo_path: deb https://repo.zextras.io/rc/ubuntu bionic main
    carbonio_domain: ubuntu.local
    carbonio_hostname: mail
    carbonio_timezone: UTC
    carbonio_zextras_password: "!23Change"
    carbonio_zextras_network: "192.168.56.0/24"

    carbonio_hosts_lines:
    - "192.168.56.13 {{ carbonio_hostname }}.{{ carbonio_domain }} {{ carbonio_hostname }}" 

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: carbonio-ce
          carbonio_hostname: localhost
          carbonio_domain: localdomain
          carbonio_timezone: America/Santiago
          carbonio_zextras_password: 123change
          carbonio_zextras_network: 192.168.0.0/24

## Access to the Web Interface

The URL to which to connect to are:

<https://mail.domain.local/> for regular user access

<https://mail.domain.local:7071/carbonioAdmin> for Administration access.

## License

MIT / BSD
